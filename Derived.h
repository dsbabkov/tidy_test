#ifndef DERIVED_H
#define DERIVED_H

#include "Base.h"

class Derived: public Base
{
public:
    Derived();
    void virtualMethod();
    void virtualMethod2();
};

#endif // DERIVED_H
